
/*

ehs.js - Script to scrape excelhighschool website.

Requires Casper JS

npm install -g casperjs

Usage: 


casperjs ehs.js <subject> --login=<user:pass> --quiz=<startnum:endnum> --question=<startnum:endnum>

casperjs ehs.js history --login=abc:123 --quiz=1234 --question=23:25 

casperjs ehs.js history   --quiz=255761:255761 --question=3:11

 casperjs ehs.js english   --quiz=250387:250387 --question=30:30

http://excelhighschool.edu20.org/question_bank/show_question/243190?position=2

casperjs ehs.js math  -quiz=243190:243190 --question=2:2


casperjs ehs.js math  

*/


var fs = require('fs');
var utils = require('utils')
var qproc = require('./qproc');
// var $ = require('JQuery');

subject_map = {

    "history": {code: "1952", title: "History"},
    "art": {code: "1907", title: "Art"},
    "business": {code: "1949", title: "Business"},
    "economics": {code: "1950", title: "Economics"},
    "engineering": {code: "2458", title: "Engineering"},
    "english": {code: "1912", title: "English"},
    "health": {code: "2331", title: "Health & Fitness"},
    "math": {code: "1905", title: "Math"},
    "other": {code: "2436", title: "Other"},
    "politics": {code: "1957", title: "Politics"},
    "psychology": {code: "1935", title: "Psychology"},
    "religion": {code: "1955", title: "Religion"},
    "science": {code: "1906", title: "Science"},
    "technology": {code: "1933", title: "Technology"}
}


if (!Array.prototype.findIndex) {
    Array.prototype.findIndex = function (predicate) {
        if (this == null) {
            throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
            throw new TypeError('predicate must be a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;

        for (var i = 0; i < length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return i;
            }
        }
        return -1;
    };
}



var casper = require('casper').create({
    clientScripts: ['jquery-2.2.3.min.js'] ,
   pageSettings: { webSecurityEnabled: false }

  // , verbose: true
  //   , logLevel: "debug"
});

casper.on('remote.message', function(msg) {
    this.echo('remote message caught: ' + msg);
});

phantom.cookiesEnabled = true;



// ARGS
var subject = subject_map[casper.cli.get(0)] // history
var subject_folder = "./output/"+  casper.cli.get(0)+"_"+subject.code // ./output/history_1952

var  login = null;
if (casper.cli.has("login")) {
    var creds = casper.cli.get("login").split(":");
    if (creds.length < 2) casper.echo("Need user and pass", 'ERROR' );
    login = {
        user: creds[0],
        pass: creds[1]
    }
}
var quizRange = null;
if (casper.cli.has("quiz")) {
    var vals = casper.cli.get("quiz")
    try {
        vals = vals.split(":"); 
        quizRange = { first: vals[0], last: vals[1]  }
    } catch(err) {
        quizRange = { first: vals , last: null  }
    }
}

var questionRange = null;
if (casper.cli.has("question")) {
    var vals = casper.cli.get("question")
    try {
        vals = vals.split(":"); 
        questionRange = { first: vals[0], last: vals[1]  }
    } catch(err) {
        questionRange = { first: vals , last: null  }
    }
}
 
var typeonly =  casper.cli.has("type-only");


casper.echo("Subject: "+ subject.code+" - "+subject.title, "INFO");
if (login) casper.echo("Login: "+ login.user+" - "+login.pass, "INFO");
if (quizRange) casper.echo("Quiz Range: "+ quizRange.first+" - "+quizRange.last, "INFO");
if (questionRange) casper.echo("Question Range: "+ questionRange.first+" - "+questionRange.last, "INFO");

//  casper.echo("typeonly: "+ typeonly, "INFO" );


var cookie_filename = "cookies.txt";
var domain = "http://excelhighschool.edu20.org";
var login_uri = (domain + "/?log_in_required=true");
bank_uri = (domain + "/library?all=true&classification=Question+bank&district=true&favorites=true&limit=200&offset=00&school=true&subject="+subject.code);



// process.exit(0);

 


// start the sequence
casper.start(login_uri, function() {
    this.viewport(900,900);
    casper.echo("Started", "INFO");
     // fs.makeDirectory(subject_folder)

       
});

/*
If skip login, we read the cookie file.
There shiould have been a run of the script with login in the last hour.
If not skipping login we write the cookie file
*/
/////////////////////////////////
if (login) {
    casper.waitForSelector("#submit_button", function pass() {
        console.log("Found form#login_form");
        this.fill('form#login_form', {
            'userid':   login.user,
            'password': login.pass
        }, true);
    }, function fail() {
        casper.echo("Could not find the login form","ERROR");
    });

    casper.waitForUrl(domain+ "/home_custom",   function () {
        this.echo('Logged in', "INFO");
    },  function(){
        this.echo("Timeout - probably not serious", "WARNING");
    },  5000   );

    casper.then(function(){
        var cookies = JSON.stringify(this.page.cookies);
        fs.write(cookie_filename, cookies, 644);
        this.echo("Saved cookies", "INFO");
    });

} else {
   casper.then(function(){
    var cookies = fs.read(cookie_filename);
    this.page.cookies = JSON.parse(cookies);
    this.echo("Read cookies", "INFO");
});


} 


/*
Open the question bank and wait for the page to finish.
We wait for particular elements to do this
Maybe not necessary
*/
casper.thenOpen(bank_uri, function openBank() {
    this.echo('GOT: '+bank_uri);
});

var selector = ".mobileOptimized > tbody"
casper.waitForSelector(selector, function pass() {
    this.echo("Found "+selector, "INFO");   
}, function fail() {
   this.echo("Could not load element "+selector, "ERROR");   
});


casper.then(function mainBlock() {



    // this.echo("Getting Quiz Links", "INFO");   
    /*
    create a list of quiz_links, containing the link and title.
    */
    var quiz_links = this.evaluate(function (JQuery) {
        var res = [];
        $(".mobileOptimized > tbody > tr > td:nth-child(3)").each(function(i, el){
            var link_el =  $(el).children("a").first();
            var span_el =  $(el).children("span").first();
            var match = $(span_el).text().match(/^[^]*(District|School)[^]*$/);
            var bank = "Undefined";
            if (match) bank = match[1];
            res.push({
                link: $(link_el).attr('href'),
                title: $(link_el).text(),
                bank: bank
            });
        });
        return res;
    });


    // this.echo("Got Raw Quiz Links", "INFO");   


    this.then(function(){
        /*
        If we are limiting to a subset of,
        alter the list
        */
        if (quizRange) {
            var firstindex = quiz_links.findIndex(function(el){
                var num = el.link.split("/").pop();
                return (parseInt(num, 10) === parseInt(quizRange.first, 10));
            });
            var lastindex = quiz_links.findIndex(function(el){
                var num = el.link.split("/").pop();
                return (parseInt(num, 10) === parseInt(quizRange.last, 10));
            });
            if (firstindex > -1){
                if (lastindex > -1) {
                    quiz_links=quiz_links.slice(firstindex, lastindex+1);
                } else {
                    quiz_links=quiz_links.slice(firstindex);
                }
            }
        }

         // utils.dump(quiz_links);



        /*
        loop over quiz_links, create the json structure (out) for each
        */
        this.eachThen(quiz_links, function processQuiz(quiz) {

            // this.echo("HERE processQuiz",  "INFO");   

            var quiz_id = quiz.data.link.split("/").pop();
            var out = {
                filename:  quiz.data.title,
                link: domain+quiz.data.link,
                title: quiz.data.title,
                category: subject.title,
                questions: []
            };

            var quiz_folder = subject_folder+"/"+out.filename
            this.then(function() {
                this.echo(("Make Directory: "+ quiz_folder+"/images"), "PARAMETER");
                fs.makeDirectory(quiz_folder);
                fs.makeDirectory(quiz_folder+"/images");
            });

            
            // utils.dump(out);

            /*
            Open the link for one quiz. There will be a list of all questions.
            wait until question list is visible         
            */
            this.thenOpen((domain+quiz.data.link), function(response) {
               this.echo('Opened Quiz; ', quiz_id, "INFO");
           });

            var selector = "#question_list"
            this.waitForSelector(selector, function pass () {
               this.echo("Found "+selector, "INFO");
           }, function fail () {
               this.echo("Could not load element "+selector, "ERROR");
           });




            this.then(function processQuestionLinks(){
                // this.echo("HERE processQuestionLinks",  "INFO");   

                /*
                Extract the list of question links into an array.
                Trim down the array if we are only interested in one question.
                */
                var question_links = this.evaluate(function (JQuery) {
                    var links = [];
                    $("tbody#question_list > tr > td:first-child > a").each(function(i, el){
                        links.push({
                            link: $(el).attr('href'),
                            title: $(el).text()
                        });
                    });
                    return links;
                });  



                if (questionRange) {
                    var firstindex = question_links.findIndex(function(el){
                        var num =  el.link.split("=").pop()
                        return (parseInt(num, 10) === parseInt(questionRange.first, 10));
                    });
                    var lastindex = question_links.findIndex(function(el){
                        var num =  el.link.split("=").pop()
                        return (parseInt(num, 10) === parseInt(questionRange.last, 10));
                    });
                    if (firstindex > -1){
                        if (lastindex > -1) {
                            question_links=question_links.slice(firstindex, lastindex+1);
                        } else {
                            question_links=question_links.slice(firstindex);
                        }
                    }
                }

                // utils.dump(question_links);


                /*
                Loop over question links, visit each question page,
                extract the question data and append it to the json 
                structure for the quiz.(out)

                also, capture any images and download sound files
                */
                this.eachThen(question_links, function questionLink(question_link) {
                    // this.echo("HERE questionLink",  "INFO");   
                    var qurl = (domain+question_link.data.link);
                    this.thenOpen(qurl, function(response) {
                        // this.echo('Opened', response.url , "INFO"); 
                    });


                    this.then(function openQuestionPage(){
                        this.wait(1000, function() {

                            var html = this.evaluate(function (JQuery) {
                                return  $("#mainContent").html();
                            });

                            this.echo(qurl,  "PARAMETER"); 

                            var question = qproc.processQuestion(html);
                            
                         
                            if (question) { 
                     
                                question = qproc.fixPaths(question, domain, qurl);
                  
                                var images = qproc.getResourceSpecs(question, "image")
                     
                                 var sounds = qproc.getResourceSpecs(question, "sound")
                        

                             
                                out.questions.push(question);
             
                                 
                                this.eachThen(sounds, function(sound){
                                    fs.makeDirectory(quiz_folder+"/sounds");
                                    var fn = (quiz_folder+"/"+sound.data.filename)
                                    this.echo(("Link: "+ sound.data.href+" -> "+ fn), "PARAMETER");                      
                                    this.download(sound.data.href,fn);
                                    this.echo(("Download finished" ), "INFO");
                                });

                         
                                this.eachThen(images, function(image) {
                                    this.echo("Selector: "+ image.data.selector+" -> "+ image.data.filename, "PARAMETER");                      
                                    this.captureSelector(quiz_folder+"/"+image.data.filename, image.data.selector,{format: "png"});
                                });
                            }
                        });
                    });
                });


                this.then(function() {
                    var fn = out.filename;
                    fs.write(quiz_folder+"/"+fn+".json",  JSON.stringify(out, null, '\t'), 644);
                });
            });
        });
    });
});

casper.run(function() {
    this.echo('Done!!', "PARAMETER").exit();
});

