#! /usr/bin/env node


/*

j2h.js - Script to convert json to html AND docx from files generated from ehs.js.

requires: handlebars, chalk, html-docx-js, seedrandom

npm install handlebars --save
npm install chalk --save
npm install html-docx-js --save
npm install seedrandom --save



Usage: j2h.js <files>

For every json input file, an html file and a word document 
will be created for both student and teacher.

Teacher files (or Master files) have (M) in the filename.
Student files have (S) in the filename 
*/

'use strict'

var fs = require('fs');
var Handlebars = require('handlebars');
var HtmlDocx = require('html-docx-js');
var seedrandom = require('seedrandom');
var chalk = require('chalk');

var should_be_zero = 0;

var infiles = process.argv.slice(2);

Handlebars.registerHelper('iftf', function(param, options) {
  if(param === "tf") {
    return options.fn(this);
  }
});
Handlebars.registerHelper('ifmc', function(param, options) {
  if(param === "mc") {
    return options.fn(this);
  }
});




infiles.forEach(function(infile){
    console.log("-------------");
    console.log("Processing: "+ infile);
    var file_prefix = infile.replace(".json", "");

    // create student and teacher html files /////////////////////////////////////
    var handlbars_source = fs.readFileSync("template.html", 'utf8');
    var handlbars_template = Handlebars.compile(handlbars_source);
    var contents = fs.readFileSync(infile, 'utf8')
    var data = JSON.parse(contents);

    // deterministic random number 
    // (seed with title so that each quiz sequence is different)
    var randgen = seedrandom(data.title);
    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(randgen() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    // shuffle answers
    data.questions.forEach(function(q){

        if (q.answers) {
            var staticAnswers = q.answers.filter(function(answer){
             var reNone =  /^None of the .* answers.*/i;
             var reAll = /^All of the .* answers.*/i 
             return (answer.text.match(reNone) || answer.text.match(reAll));
         });
            
            var realAnswers = q.answers.filter(function(answer){
             var reNone =  /^None of the .* answers.*/i;
             var reAll = /^All of the .* answers.*/i 
             return (!(answer.text.match(reNone) || answer.text.match(reAll)));
         });
            realAnswers = shuffle(realAnswers);
            
            q.answers =  realAnswers.concat(staticAnswers);
        }
    });

    // make a copy each for master and student in html, docx, pdf
    /////////////////////////////////////
    ["master", "student"].forEach(function(who){    
        data.master = (who === "master");
        var html_result = handlbars_template(data);
        var out_prefix = file_prefix+"("+who[0].toUpperCase()+")";
        fs.writeFileSync((out_prefix+".html"), html_result);
        // var docx_result = HtmlDocx.asBlob(html_result);
        // fs.writeFileSync((out_prefix+".docx"), docx_result);
        console.log("-> "+chalk.green(out_prefix+".html"));
        // console.log("-> "+chalk.green(out_prefix+".docx"));
    });
    /////////////////////////////////////
    console.log("-------------");
})


