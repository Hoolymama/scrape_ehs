#! /usr/bin/env node

var fs = require('fs');
var utils = require('utils')
var qproc = require('./qproc');
//var $ = require('JQuery');


var casper = require('casper').create({  
  // verbose: true
  // , logLevel: "debug"
});


casper.start("localhost", function() {
    casper.echo("Started", "INFO");


var mc = '<div id="centreColumn"><div id="alerts" style="display:none"></div>\n<div class="pageHeading mobile_only"><h2 id="mobile_page_heading">Library</h2><h1>Question 3</h1><img src="/images/resources/new_question_bank.png" alt="" id="fromLeft"></div>\n<div class="optionsRibbon optionsRight" style="overflow: visible; display: block;"><ul><li><a title="Previous" href="/question_bank/show_question/255761?position=2"><i class="arrowLeft single"></i><span class="hide">Previous</span></a></li><li><a title="Return to question bank" href="/question_bank/cancel/255761"><i class="list single"></i><span class="hide">Return to question bank</span></a></li><li><a title="Next" href="/question_bank/show_question/255761?position=4"><i class="arrowRight single"></i><span class="hide">Next</span></a></li><li class="mobile_only" style="display: none;"><div class="dropDownHolder"><a href=""><i class="cog"></i><i class="arrowDown"></i></a><div class="dropDown" style="right: auto; left: -1px;"><ul></ul></div></div></li></ul></div><style type="text/css">.optionsRibbon ul li {padding: 2px 3px;}</style>\n<div class="clearfix"></div><h1>Multiple choice (one answer)</h1>\n<p>\nThe kingdom of Axum __________\n</p>\n<dl>\n <dt>1. <span><i class="tick"></i><span class="textOffScreen">Yes</span></span></dt>\n<dd>all of these answers.</dd>\n<dt>2. <span><i class="xCross"></i><span class="textOffScreen">No</span></span></dt>\n<dd>brought about the decline of the Kush.</dd>\n<dt>3. <span><i class="xCross"></i><span class="textOffScreen">No</span></span></dt>\n<dd>emerged as an independent state that combined Arab and African cultures.</dd>\n<dt>4. <span><i class="xCross"></i><span class="textOffScreen">No</span></span></dt>\n<dd>competed with Kush over control of the ivory trade.</dd>\n</dl>\n<p>Points: 1</p>\n<p>\nDisplay: List\n·\nOrdering of choices: Randomize\n</p>\n</div>'

var tf = '<div id="centreColumn">\n<div id="alerts" style="display:none"></div>\n<div class="pageHeading mobile_only"><h2 id="mobile_page_heading">Library</h2><h1>Question 12</h1><img src="/images/resources/new_question_bank.png" alt="" id="fromLeft"></div>\n<div class="optionsRibbon optionsRight" style="overflow: visible; display: block;"><ul><li><a title="Previous" href="/question_bank/show_question/250361?position=11"><i class="arrowLeft single"></i><span class="hide">Previous</span></a></li><li><a title="Return to question bank" href="/question_bank/cancel/250361"><i class="list single"></i><span class="hide">Return to question bank</span></a></li><li><a title="Next" href="/question_bank/show_question/250361?position=13"><i class="arrowRight single"></i><span class="hide">Next</span></a></li><li class="mobile_only" style="display: none;"><div class="dropDownHolder"><a href=""><i class="cog"></i><i class="arrowDown"></i></a><div class="dropDown" style="right: auto; left: -1px;"><ul></ul></div></div></li></ul></div><style type="text/css">.optionsRibbon ul li {padding: 2px 3px;}</style>\n<div class="clearfix"></div><h1>True or false</h1>\n<p>\nAccording to the rules when writing numbers, is the following sentence correct?\n<br>\n<br>It took 4 hours to proofread all 75 pages of the manuscript.\n</p>\n<p>Answer? <span><i class="xCross icnColor"></i><span class="textOffScreen">No</span></span></p>\n<p>Points: 1</p>\n<p>Correction box? <span><i class="xCross icnColor"></i><span class="textOffScreen">No</span></span></p>\n</div>'


  var html = tf;

  var question = qproc.processQuestion(html);

  utils.dump(question)
  // this.echo(question, "PARAMETER")
});

casper.run();








