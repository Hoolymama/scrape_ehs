

var $ = require('JQuery');

$.fn.extend({
    getPath: function () {
        var path, node = this;
        while (node.length) {
            var realNode = node[0], name = realNode.localName;
            if (!name) break;
            name = name.toLowerCase();

            var parent = node.parent();

            var sameTagSiblings = parent.children(name);
            if (sameTagSiblings.length > 1) { 
                allSiblings = parent.children();
                var index = allSiblings.index(realNode) + 1;
                if (index > 1) {
                    name += ':nth-child(' + index + ')';
                }
            }

            path = name + (path ? '>' + path : '');
            node = parent;
        }

        return path.replace(/^div/,"div#centreColumn" );
    }
});


var uuid = function(chars) {
    var d = new Date().getTime();
    var uid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    if (chars){
        return uid.slice(0, chars);
    } else{
        return uid;
    } 
}


var numberFromWord = function(word){
    var words = ["zero", "one", "two", "three", "four", "five", 
    "six", "seven", "eight", "nine", "ten"]
    return words.indexOf(word);
}

var getQuestionType = function(elem){
    var text = elem.children("h1").first().text().trim();
    var regexes = {
        "mc"  : /^[^]*multiple choice[^]*$/i  ,
        "tf" : /^[^]*true or false[^]*$/i    
    }
    for (key in regexes) {
        if (text.match(regexes[key])) {
            return key;
        }
    }
    return "unknown";
}

var mcGetNumCorrect = function(elem){
    var result = "one";
    var  typeText = elem.children("h1").first().text().trim();
    var match = typeText.match(/^[^]*multiple choice \(([\w]+)[^]*$/i);
    if (!!match) {
        result= match[1]; 
    }
    return result;
}


var imgFromElements = function(element){
    var result = null;
    var img = $(element).find("img");
    if ($(img).length) {
        var selector = $(img).first().getPath();
        result = {
            selector: selector,
            filename: ("images/"+uuid()+".png")
        }
    }
    return result;
}

var soundFromElements = function(element) {
   var result = null;
   var sound = $(element).find('a[href$=".mp3"]'); 
   if ($(sound).length) {
    var selector = $(sound).first().getPath();
    var  name = $(sound).text()

    result = {
        name: name,
        href:  sound.attr("href"),
        filename: ("sounds/"+uuid()+".mp3")
    }
}
return result;
}

var mcGetAnswers = function(elem){
    var result = [];
    var dl_children = elem.children("dl").children();
    var num_children =  dl_children.length;
    for (var i = 0;i < num_children; i+=2) {
        var j = i+1
        var dt = $(dl_children).eq(i);
        var dd = $(dl_children).eq(j);

      // only want text except that found in hrefs
      // http://stackoverflow.com/a/8851526/179412
      var text = dd.clone().children(".resource-jwplayer").remove().end().text().trim();

      var answer = {
        text: text,
        correct: dt.find("i").hasClass("tick")
    }

    var img
    if (img = imgFromElements(dd)) { answer.image = img; }

    var sound
    if ( sound = soundFromElements(dd)) { answer.sound = sound; }


    result.push(answer);
}
return result;
}

var getPoints = function(text){
    var pointsmatch = text.match(/^Points:\s+([0-9]+)$/);
    if (pointsmatch) {
        return parseInt(pointsmatch[1], 10);    
    }
    return null;
}


var getParagraphElements = function(elem){
    return elem.children("p:not([class])").filter(function(i){
        return !!$.trim($(this).html()) ;
    });
}


var getParagraphOrDivElements = function(elem){
    return elem.children("p:not([class]), div:not([class])").filter(function(i){
        return !!$.trim($(this).html()) ;
    });
}

var textFromParagraph = function(paragraph){
    return $(paragraph).text().replace(/\u00a0/g, " ").trim();
}

var handler =  {

    mc: function(elem){
        // console.log("HANDLER")
        var result = {}
        result.type = "mc";
        result.numCorrect = mcGetNumCorrect(elem);

        var p_elements = getParagraphOrDivElements(elem);
        var question_parts = [];

        $(p_elements).each(function(i){
            var text = textFromParagraph(this);
            var pts = getPoints(text);
            if (pts) { result.points = pts; return false; }
            if (text)   question_parts.push(text)

        });

        result.question = question_parts.join("\n\n") || "";

        var img
        if ( img = imgFromElements(p_elements)) { result.image = img; }

        var sound
        if ( sound = soundFromElements(p_elements)) { result.sound = sound; }

        result.answers = mcGetAnswers(elem);
        return result;
    },






    tf: function(elem){
        var result = {}
        result.type = "tf";

        var p_elements = getParagraphOrDivElements(elem);
        var question_parts = [];

        $(p_elements).each(function(i){
            var text = textFromParagraph(this);

            var pts= getPoints(text);
            if (pts) { result.points = pts; return false; }

            var answermatch = text.match(/^Answer\?.*$/);
            if (answermatch) {
                result.answer =  $(this).find("i").hasClass("tick");               
            } else {
                if (text)   question_parts.push(text);
            }
        });

        result.question = question_parts.join("\n\n") || "";

        var img
        if ( img = imgFromElements(p_elements)) { result.image = img; }
        return result;
    },

    unknown: function(){
        return {question: "Unknown Question Type"};
    }

}



exports.processQuestion = function(html) {
    var elem = $(html);
    var type = getQuestionType(elem);
    var result;
 
    try {
        return handler[type](elem);
    }
    catch (err) {
        console.log("ERROR:" +  err.message);
        return {error: err.message}
    }
}

exports.fixPaths = function(question, domain, question_url) {
    if (!!question.answers) {
        question.answers.forEach(function(a){
            if (!!a.sound) {
                a.sound.href = (domain+a.sound.href );
            }
        }); 
    }
    if (question.sound) {
        question.sound.href = (domain+question.sound.href );
    }
    question.url = question_url;
    return question
}

exports.getResourceSpecs = function(question, resource){
    try {
        var result = [];
        if (!!question.answers) {
            question.answers.forEach(function(a){
                if (!!a[resource]) {
                    result.push(a[resource]);
                }
            }); 
        }
        if (question[resource]) {
            result.push(question[resource]);
        }
        return result;           
    }
    catch (err) {
        console.log("ERROR:" +  err.message);
        return {error: err.message}
    }
        
}
